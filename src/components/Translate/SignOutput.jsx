import { useState } from "react"
import SignImage from "./SignImage"





const SignOutput = (props) =>{

    const lettersList = props.bildene.map((bokstaven) =>{

        return(
            <SignImage bildet={bokstaven} key={bokstaven}/>
        )
    })
    
    return(
        <>
        <h1>Output</h1>
        {lettersList}
        </>
        )

}


export default SignOutput 