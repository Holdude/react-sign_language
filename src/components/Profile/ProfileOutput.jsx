

const ProfileOutput = (props) =>{

    // listen
    // Number of words presented
    let wordsRepresented = 0

    const mapListen = props.ordene.map((trans) =>{

        // Checks if more than 10 presented
        if(wordsRepresented<=9){
            wordsRepresented++
            return(
                <div key={wordsRepresented}>
                    <p>{trans}</p>
                </div>
            )
        }
    })
    


    return(
        <>
        <h2>Last 10 translations</h2>
        {mapListen}
                </>
    )
}


export default ProfileOutput