import { addTranslation, checkTranslist, deleteTranslations } from "../api/user"
import withAuth from "../hoc/withAuth"
import { useEffect, useState } from "react"
import ProfileOutput from "../components/Profile/ProfileOutput"
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storage";
import ProfileActions from "../components/Profile/ProfileActions";
import {useForm} from "react-hook-form"
import '../Css/profileCss.css';

// array for holding the outputs
let listen = []

// Check for if output is updated
let isUpdated = false

const Profile = () => {

    // local states
    const [words, setWords] = useState([])
    const {register, handleSubmit, formState:{errors}} = useForm()

    // getting the translated words
    const transene = async () => {
        isUpdated = false
        const iden = storageRead(STORAGE_KEY_USER).id
        const Ordlisten = await checkTranslist(iden)
        listen = Ordlisten[0].translations
        setWords(listen)

    }

    // Deleting last 10 words
    function deleteIt(){
        isUpdated = false
        // Sorts into 2 arrays, delete array and keep aray
        deleteTranslations()
        // Commits the arrays to the api
        transene()
    }

    //
    useEffect(() => {
        // If not updated update
        if (!isUpdated) {
            isUpdated = true
            transene()
            
        }


    })

    return (
        <>
            <h1>Profile</h1>
            <div className="outputProfile"> 
                <ProfileOutput ordene={listen} />
            </div>
            
            <button onClick={deleteIt}>Delete records</button>
        </>
    )
}


export default withAuth(Profile)
