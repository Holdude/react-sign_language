import SignOutput from "../components/Translate/SignOutput"
import { useEffect, useState } from "react"
import {useForm} from "react-hook-form"
import { findLetters } from "../api/letters"
import {getandSaveTransList } from "../api/user"
import withAuth from "../hoc/withAuth"
import '../Css/translateCss.css';

let arrayLetter = []


// state letters
const Translator = () => {
    const [letters, setLetters] = useState([])

    const {register, handleSubmit, formState:{errors}} = useForm()
    
    const onSubmit =  data =>{
        const arr = findLetters(data.wordTrans)
        //setLetters(arr[0].name)
        setLetters(arr)
        getandSaveTransList(data.wordTrans)
    }

    
    useEffect(() => {
      arrayLetter = []
       letters.forEach(leter => {
         arrayLetter.push(leter)  
         
       })

    })

    return(
        
        <>
        <h1>Translator</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="text" {...register("wordTrans")} placeholder="Type word to be translated"/>
            <button type="submit" className="submitButton">Translate</button>
            
        </form>
        <div className="outputClass">
          <SignOutput bildene={arrayLetter}/>
        </div>
        </>
        )
}


export default withAuth(Translator)