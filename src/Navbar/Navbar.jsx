import { NavLink } from "react-router-dom"
import { useUser } from "../context/UserContext"
import ProfileActions from "../components/Profile/ProfileActions";
import '../Css/navbarCss.css';

const Navbar = () => {

    const { user } = useUser()

    return (
        <nav>
            { user != null &&
                <ul>
                    <li>
                        <NavLink to="/translator" >Translator</NavLink>
                    </li>
                    <li>
                        <NavLink to="/profile" >Profile</NavLink>
                    </li>
                    <li>
                        <ProfileActions />
                        </li>
                </ul>     
            }
            </nav>
    )
}
export default Navbar