import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storage";
import { createHeaders } from "./index"

const apiUrl = "https://noroff-assignment-api-erlend.herokuapp.com/translations"
const apiKey = "L9M22VKQ09XY6B4NPMRL4VWOBX2QN9SVQXO6O87H3LZR334OFTNV3AT43WR6IXK9"

let loading = false;

// Check for user
const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [ null, data ]
    }
    catch (error) {
        return [ error.message, [] ]
    }
}


// create a user
const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST', // Create a resource
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: [] 
            })
        })
        if (!response.ok) {
            throw new Error('Could not create user with username ' + username)
        }
        const data = await response.json()
        return [ null, data ]
    }
    catch (error) {
        return [ error.message, [] ]
    }
}

            

// Send in the translations as a array
export const addTranslation = async (trans) =>{


    const iden =storageRead(STORAGE_KEY_USER).id


    const response = await fetch(apiUrl+"/"+iden,{
        method: 'PATCH',
        headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            translations: trans
        })
    })

    // Test if response is okay
    if(!response.ok){
        throw new Error("Could not update translations")
    }


}

// add the new word to the api
export const getandSaveTransList = async(theWord) =>{

    const userList = await checkTranslist(storageRead(STORAGE_KEY_USER).id)


    // Add the new word on the top

    if(loading){
        
    } else {

        let newAr = userList[0].translations
        newAr.unshift(theWord)
        addTranslation(newAr)
    }
}


// Log in user
export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)

    if (checkError !== null) {
        return [ checkError, null ]
    }

    if (user.length > 0) {
        // user does NOT exist.
        return [ null, user.pop() ]
    }

    return await createUser(username)

}

// getting the translations list from selected user
export const checkTranslist = async (username) => {
    loading= true
    try {
        const response = await fetch(`${apiUrl}/${username}`)
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        loading = false

        return [data]
        
    }
    catch (error) {
        return [ error.message, [] ]
    }
}

// "Deleting" past translations

export const deleteTranslations = async () =>{
    const iden =storageRead(STORAGE_KEY_USER).id

    const theList = await checkTranslist(iden);

    
    let wordCount = 0

    let deleteArray = []
    let keepArray = []

    // Take the top 10 and move to an "deleted" array

    theList[0].translations.forEach(element => {


        // Check counter
        if(wordCount <= 9){
            wordCount++
            deleteArray.push(element)

        } else{
            keepArray.push(element)
        }
    });

    addDeleted(keepArray, deleteArray)

    // 
    

}

// Send in as array, both deleted and keeping
export const addDeleted = async (keepArray, deleteArray) =>{

    const iden =storageRead(STORAGE_KEY_USER).id


    const response = await fetch(apiUrl+"/"+iden,{
        method: 'PATCH',
        headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            translations: keepArray,
            deletedTrans: deleteArray
        })
    })

    // Test if response is okay
    if(!response.ok){
        throw new Error("Could not update translations")
    }


}