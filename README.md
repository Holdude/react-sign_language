#Sign language translator
Created by https://gitlab.com/stianstian95 & https://gitlab.com/Holdude
A react project that lets you translate natural language to sign language.

api can be found here: https://noroff-assignment-api-erlend.herokuapp.com/translations
and the heroku app here: https://sleepy-fjord-97309.herokuapp.com/

## About this project
The game consists of three pages, login page to create user, translator page and 
a profile page.

## Installation
To start this on your own computer clone the project and run the following commands
in root foolder.

```
npm install
```

```
npm start
```

## Register user section
This is where the the user can register a name and gets taken to the next page (translator)

## Translator section
Here the user can type in words that will be translated and displayed using images.
This translation will then be saved to the users api.

## Profile section
This screen shows the users profile with the last 10 translations the user typed.
Here you can also delete the history or log out. 
Deleting will remove the 10 last translations from the "translation" array and move them to the "deleted" array

## Attribution
Would like to thank Dewalds for his contribution regarding the videoes posted in 
the course.
W3Schools for css
